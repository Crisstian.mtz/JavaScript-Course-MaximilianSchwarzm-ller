var aNumber = 5;
console.log(aNumber);
var anotherNumber = aNumber;
console.log(anotherNumber);
aNumber = 12;
console.log(aNumber);
console.log(anotherNumber);
/*Tipo primitivo (numbers, strings, float points). Un valor se copia y se almacena con otro nombre, de tal forma que aunque se modifique
el valor original, el valor copia mantendra el antiguo valor*/

var array = [1, 2, 3];
var anotherArray = array;
console.log(array);
console.log(anotherArray);
array.push(4);
console.log(array);
console.log(anotherArray);
/*Los objetos son valores de referencia, por lo que guarda los valores del objeto de referencia (mediante un puntero) y si cambio el
objeto original, se cambia la referencia tambien. Push se encarga de agregar un elemento al final de un vector*/

/*VECTORES*/

console.log(array.length);
/*Muestra la longitud del vector*/

array[1] = 100;
console.log(array[1]);
/*Accede al elemento indicado del vector*/
array[5] = 100;
console.log(array)

array.forEach(function(element){
    console.log(element);
});
/*Separa cada elemento del vector*/

console.log(array.pop());
/*Elimina el ultimo elemento de una matriz*/

console.log(array.shift());
/*Elimina el primer elemento del vector*/

array.unshift('New');
/*Agrega un nuevo elemento al principio del vector*/
console.log(array);

console.log(array.indexOf('New'));
/*Arroja la ubicación del elemento deseado dentro del vector*/

var newArray = array.splice(2, 2);
/*Corta el vecctor a partir de la ubicacion marcada. El segundo numero indica la cantidad de elementos
que se seleccionaran*/
console.log(array);
console.log(newArray);

var newArray2 = array.slice(1);
console.log(newArray2);
/*Copia los elementos seleccionados del vector sin afectar al vector principal*/

array = [1, 2, 3, 4];
console.log(array.filter(function(value){
    return value > 2;
}));
/*Filtra los valores, en este caso arroja los mayores a 2*/

console.log(array.map(function(value){
    return value * 2;
}));
/*Imprime un nuevo vector sin modificar el original*/

console.log(array.reverse());
/*Invierte el vector original*/

var newArray3 = [5, 6];
console.log(array.concat(newArray3));
/*Une dos vectores sin alterar ninguno de los originales*/

console.log(array.join(', '));
/*Combierte un vector en string y solo indica como separar el vector. No modifica el vector original*/

console.log(array.reduce(function(total, value){
    return total + value;
}));
/*Reduce un vector a un solo valor. Return indica la logica que debe seguir para dicha reduccion*/
