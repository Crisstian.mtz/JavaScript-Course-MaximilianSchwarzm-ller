/*OBJETOS*/

var person = {
    name: 'Max',
    age: 27,
    details: {
        hobies: ['sports', 'coocking'],
        location: 'Germany'
    },
    greet: function() {
        console.log('Hello!, I am ' + this.age + ' years old');
    }
};
console.log(person);
/*Los objetos tienen varias variables dentro de si. Incluso se puede guardar objetos y funciones dentro de un objeto*/
console.log(person.name);
console.log(person.details.hobies);
/*Se puede acceder usando el nombre de la variable*/
console.log(person['name']);
/*O con notacion tipo vector*/
person.greet();
/*Permite ejecutar funciones dentro de un objeto. This se usa para llamar un elemento dentro de un objeto*/
person.name = 'Anna';
console.log(person);
/*Permite cambiar valores de los objetos*/

var anotherPerson = new Object();
anotherPerson.name = 'Anna';
anotherPerson.age = 30;
/*Otra forma de crear un objeto en la que los parametros se agregan despues*/
console.log(anotherPerson);

var anotherPerson1 = Object.create(person);
anotherPerson1.name = 'Concha';
/*Otra forma de crear objetos empleada en prototipos. A pesar de que anotherPerson1 no tiene edad,
se puede obtener del prototipo agregado de person*/
console.log(anotherPerson1.age);

function Person(name, age){
    this.name = name;
    this.age = age;
};
Person.prototype.greet = function(){
    console.log('Hello');
}
var person2 = new Person('Max', 27);
console.log(person2);
person2.greet();
/*Otra forma de construir objetos en la que Person.prototype es el prototipo del objeto*/


/*FUNCIONES*/

/*Closures*/

function generator(input){
    var number =input;
    return function(){
        return number * 2;
    };
}
var calc = generator (900);
console.log(calc());
/*Permite guardar en una variable una funcion escrita en un scope inferior, para ser usada posteriormente
en un scope superior*/

/*Immediately invoked functions executions*/
(function calc(){
    var number = 10;
    console.log(number);
})();
/*Sirven para ejecutar una función en un scope local y así evitar usar el scope global (porque hay variables que 
interfieren o podrian sobreescribirse). Si hago console.log fuera de la función, saldrá error ya que number no 
está definido en el scope superior*/

/*Argumentos*/
function message(message){
    console.log(message);
    console.log(arguments[1]);
    /*Me permite acceder a determinado argumento*/
}
message('Hi!', 10);
var msg = message;
console.log(msg.name);
/*.name accede al nombre de la funcion*/


/*BUILT IN FUNCTIONS*/

/*Set timeout*/
setTimeout(function(){
    console.log('Finished!');
},3000);
/*Se ejecuta despues del tiempo (milisegundos) marcado*/

/*set interval*/
var interval = setInterval(function(){
    console.log('Ping!');
}, 500);
setTimeout(function(){
    clearInterval(interval);
}, 2500);
/*Ejecuta la función cada determinado tiempo (especificado en milisegundos)*/


/*TRANSFORMING*/

var a = '5';
var b = 'FBB123';
console.log(parseInt(a));
/*Transforma string en number*/
console.log(parseInt(b, 16));
/*Tambien puede transformar hexadecimales especificando que base tienen (16)*/
var c = 10.3;
console.log(c.toString());
/*Es posible cambiar de number a string*/
console.log(c.toFixed(2));

/*Redondea el numero al numero de decimales especificados*/
var string = ('Any text     ');
console.log(string.length);
/*Arroja el numero de letras que componen un string, incluyendo espacios*/
console.log(string[2]);
console.log(string.charAt(1));
/*Se puede acceder a cualquier elemento de un string de esta forma*/
console.log(string.concat(' add a new new string'));
/*Agrega string a otro string*/
console.log(string.toUpperCase());
/*Vuelve todo a mayusculas: LowerCase para minusculas*/
console.log(string.split(' '));
/*Divide un string en varios elementos desde el caracter seleccionado*/
console.log(string.trim());
/*Elimina espacios en blanco antes o despues del texto, no elimina espacios dentro del string*/

var pi = Math.PI;
console.log(pi);
/*Valores matemáticos como pi, e, abs, round, ceil (redondea al superior), floor (redondea al inferior),
exp, E (logaritmo natural), Max (valor maximo de una lista), min (valor minimo), random, (numero entre 0 y 1),
etc.*/

var today = new Date();
console.log(today.toString());
/*Arroja la fecha de hoy*/
var newDate = new Date(1995, 9, 1);
console.log(newDate.toString());
/*Arroja la fecha deseada, donde 0 es igual a enero. También se puede ingresar como '2016/05/20'*/
console.log(today.getDate());
/*Arroja la fecha. Day arroja dia de la semana comenzando por domingo=0, etc.*/

var string = 'abc';
var pattern = /ab/;
console.log(pattern.exec(string));
/*Indica si el patron /ab/ se encuentra en la variable string. la cual debe tener la misma estructura ab.
Sirve para validar información*/


/*Document Object Model DOM*/

console.log(window.innerWidth);
/*Muestra ancho de la pagina web incluyendo scrolling.Funciona con innerHeight*/
console.log(window.outerWidth);
/*Lo mismo que inner pero se le suman anchos y bordes del navegador*/

console.log(document.getElementsByClassName('Simple'));
/*Acceder a elementos en el html mediante nombres de clase*/
console.log(document.querySelectorAll('.Simple')[0]);
/*Otra forma de seleccionar elementos de html. All muestra todos los elementos, sin all muestra
solo el primer elemento. Paca class se usa ., para id se usa #. Usando [] se puede seleccionar
la posicion de cierto elemento*/

/*alert('Alerta');*/
/*Manda alerta en el navegador*/
/*console.log(confirm('Are you sure?'));*/
/*Abre una alerta que permite al usuario seleccionar si (true) o no (false(*/
/*console.log(prompt('Your name?'));*/
/*Ingresa un input escrito por el usuario*/