/*EVENTS*/

window.onload = function(){
    console.log('Loaded!');
};
/*Se ejecuta cuando se carga la pantalla*/
var btn1 = document.querySelector('.button1');
btn1.onclick = function(){
    console.log('Clicked!');
};
/*Indica si se hace click en un boton. Si se ponen dos o mas event listener, se sobreescribe la accion y solo
se mostrara lo ultimo*/
var btn2 = document.querySelector('.button2');
btn2.addEventListener('click', listener1);
btn2.addEventListener('click', listener2);
setTimeout(function(){
    btn2.removeEventListener('click', listener1);
}, 2000);
function listener1(){
    console.log('listener 1');
};
function listener2(){
    console.log('listener 2');
};
/*De esta forma se pueden agregar varias acciones ante un solo evento*/
var inner = document.querySelector('#inner');
inner.addEventListener('click', innerListener);
function innerListener(event){
    /*event.stopProapation();*/
    console.log('Click inner');
};
var outer = document.querySelector('#outer');
outer.addEventListener('click', outerListener);
function outerListener(event){
    console.log('Click outer');
};
/*Si ambos div estuvieran encimados, se activarian ambos eventos, eso se corrije 
usando stopPropagation*/


/*AJAX*/
/*
var http = new XMLHttpRequest();
var url = 'https:jsonplaceholder.typicode.com/posts';
var method = 'GET';
http.open(method, url);
http.onreadystatechange = function(){
    if (http.readyState === XMLHttpRequest.DONE && http.status === 200){
        console.log(http.responseText);
    }
    else if (http.readyState === XMLHttpRequest.DONE && http.status !== 200){
        console.log('Error');
    }
};
http.send();
*/
















